import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {
    private Player player;
    private Ball[] balls;

    Canvas(Player player, Ball[] balls) {
        setFocusable(true);
        requestFocusInWindow();
        this.player = player;
        this.balls=new Ball[balls.length];
        this.balls = balls;
        this.addKeyListener(player.getController());
        setPreferredSize(new Dimension(500,500));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        player.drawPlayer(g);
        for (Ball ball : balls) {
            ball.drawBall(g);
        }
        repaint();
    }
}
