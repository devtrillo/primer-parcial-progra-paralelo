import java.awt.*;
import java.util.Random;

class Ball {
    private int x, y, angle, size, speed;
    private BallThread ballThread;
    private Color color;

    Ball(int id, int x, int y, Player player, Score score) {
        this.x = x;
        this.y = y;
        size = 50;
        speed = 3;
        this.angle = new Random().nextInt(360);
        this.color = Color.darkGray;
        ballThread = new BallThread(id, this, player, score);
        Thread t = new Thread(ballThread);
        t.start();
    }

    int getSize() {
        return size;
    }

    void setColor(Color color) {
        this.color = color;
    }

    void increaseSize() {
        size++;
    }

    int getAngle() {
        return angle;
    }

    void setAngle(int angle) {
        this.angle = angle % 360;
    }

    int getSpeed() {
        return speed;
    }

    int getXPos() {
        return x;
    }

    int getYPos() {
        return y;
    }

    void setDeltaPosition(int deltaX, int deltaY) {

        x += deltaX;
        y += deltaY;

        if (x < 20) {
            x = 20;
        } else {
            x = Math.min(x, 430);
        }

        if (y < 20) {
            y = 20;
        } else {
            y = Math.min(y, 430);
        }
    }

    void drawBall(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(3));
        g2.setColor(color);
        g2.drawOval(x, y, size, size);
        g2.setColor(Color.BLACK);
        g2.drawString(ballThread.getStatus(), x + (size / 2) - 24, y + size / 2);
    }


}
