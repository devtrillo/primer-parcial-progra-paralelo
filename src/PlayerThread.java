public class PlayerThread extends Thread {
    private Player player;
    private int speed = 3;

    PlayerThread(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i == 99) i = 0;
            player.setPosition(getNewXPosition(), getNewYPosition());
            try {
                sleep(33);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int getNewXPosition() {
        int direction = player.getDirection();
        if (direction == 1 || direction == 7 || direction == 4)
            return -speed;
        else if (direction == 3 || direction == 9 || direction == 6)
            return speed;
        else return 0;
    }

    private int getNewYPosition() {
        int direction = player.getDirection();
        if (direction == 1 || direction == 3 || direction == 2)
            return -speed;
        else if (direction == 7 || direction == 9 || direction == 8)
            return speed;
        return 0;
    }

}
