import java.awt.*;

class Player {
    private int x, y, direction;
    private PlayerController controller;

    /* Direction
    1 2 3
    4   6
    7 8 9 */
    Player(int x, int y) {
        this.x = x;
        this.y = y;
        direction = 5;
        controller = new PlayerController(this);
        PlayerThread thread = new PlayerThread(this);
        thread.start();
    }

    PlayerController getController() {
        return controller;
    }

    int getDirection() {
        return direction;
    }

    void setDirection(int direction) {
        if (direction <= 9 && direction >= 1)
            this.direction = direction;
    }

    int getXPos() {
        return x;
    }

    int getYPos() {
        return y;
    }

    void setPosition(int deltaX, int deltaY) {
        x += deltaX;
        y += deltaY;

        if (x < 20) x = 20;
        else x = Math.min(x, 480);

        if (y < 20) y = 20;
        else y = Math.min(y, 450);
    }

    void drawPlayer(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(3));

        int size = 5;
        switch (direction) {
            case 1://Draw up left
                g2.drawLine(x, y, x + size, y);
                g2.drawLine(x, y, x, y + size);
                break;
            case 2:
            case 5:
                g2.drawLine(x, y, x + size, y + size);
                g2.drawLine(x, y, x - size, y + size);
                break;
            case 3:
                g2.drawLine(x, y, x - size, y);
                g2.drawLine(x, y, x, y + size);
                break;
            case 4:
                g2.drawLine(x, y, x + size, y + size);
                g2.drawLine(x, y, x + size, y - size);
                break;
            case 6:
                g2.drawLine(x, y, x - size, y - size);
                g2.drawLine(x, y, x - size, y + size);

                break;
            case 7:
                g2.drawLine(x, y, x + size, y);
                g2.drawLine(x, y, x, y - size);
                break;
            case 8:
                g2.drawLine(x, y, x - size, y - size);
                g2.drawLine(x, y, x + size, y - size);
                break;
            case 9:
                g2.drawLine(x, y, x - size, y);
                g2.drawLine(x, y, x, y - size);
                break;
            default:
                break;
        }
    }
}
