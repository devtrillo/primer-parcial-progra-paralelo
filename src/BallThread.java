import java.awt.*;
import java.util.Random;

public class BallThread implements Runnable {
    private int id;
    private String status;
    private Player player;
    private Ball ball;
    private Score score;
    private boolean touched;

    BallThread(int ID, Ball ball, Player player, Score score) {
        status = "Created";
        this.id = ID;
        this.ball = ball;
        this.score = score;
        this.player = player;
        touched = false;
    }

    @Override
    public void run() {
        int i = 0;
        while (!touched) {
            if (i == 99) i = 0;

            if (i % 8 == 0) ball.setAngle(ball.getAngle() + new Random().nextInt(40));
            status = "Running" + id;
            ball.setDeltaPosition(calculateDeltaXPos(), calculateDeltaYPos());
            calculateCollision();
            score.incrementPoints();
            ball.increaseSize();
            try {
                Thread.sleep(40);
                status = "Sleeping";
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
        status = "Finished";
    }

    private void calculateCollision() {
        int playerX = player.getXPos();
        int playerY = player.getYPos();
        int ballSize = ball.getSize();
        int ballX = ball.getXPos() + (ballSize / 2);
        int ballY = ball.getYPos() + (ballSize / 2);
        double distance = Math.sqrt(Math.pow(playerX - ballX, 2) + Math.pow(playerY - ballY, 2));
        if (distance < (double) ballSize / 2) {
            ball.setColor(Color.GREEN);
            touched = true;
        }

    }

    private int calculateDeltaXPos() {
        int angle = this.ball.getAngle();
        int speed = this.ball.getSpeed();
        return (int) Math.round(Math.cos(angle)) * speed;
    }

    private int calculateDeltaYPos() {
        int angle = this.ball.getAngle();
        int speed = this.ball.getSpeed();
        return (int) Math.round(Math.sin(angle)) * speed;
    }

    String getStatus() {
        return status;
    }
}
