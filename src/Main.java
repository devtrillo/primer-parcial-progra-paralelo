import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Player player = new Player(100, 100);
        Ball[] balls = new Ball[10];
        Score score = new Score();
        for (int i = 0; i < balls.length; i++) {
            int x, y;
            x = new Random().nextInt(500);
            y = new Random().nextInt(500);
            balls[i] = new Ball(i, x, y, player, score);
        }
        Canvas c = new Canvas(player, balls);
        new Window(c, score);
    }
}
