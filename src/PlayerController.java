import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PlayerController implements KeyListener {
    private Player player;
    private boolean[] keysPressed = new boolean[4];

    PlayerController(Player player) {
        System.out.println("CREATED");
        this.player = player;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    /*
     0
    1 2
     3
     */
    @Override
    public void keyPressed(KeyEvent keyEvent) {
        handleKeyEvent(true, keyEvent.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        handleKeyEvent(false, keyEvent.getKeyCode());
    }

    private void handleKeyEvent(boolean isKeyDown, int keyPressed) {
        if (keyPressed == KeyEvent.VK_UP || keyPressed == KeyEvent.VK_W)
            keysPressed[0] = isKeyDown;
        else if (keyPressed == KeyEvent.VK_LEFT || keyPressed == KeyEvent.VK_A)
            keysPressed[1] = isKeyDown;
        else if (keyPressed == KeyEvent.VK_RIGHT || keyPressed == KeyEvent.VK_D)
            keysPressed[2] = isKeyDown;
        else if (keyPressed == KeyEvent.VK_DOWN || keyPressed == KeyEvent.VK_S)
            keysPressed[3] = isKeyDown;
        calculateDirection();
    }

    /* Direction
    1 2 3
    4 5 6
    7 8 9 */
    private void calculateDirection() {
        if (keysPressed[0] && keysPressed[1])
            player.setDirection(1);
        else if (keysPressed[0] && keysPressed[2])
            player.setDirection(3);
        else if (keysPressed[3] && keysPressed[1])
            player.setDirection(7);
        else if (keysPressed[3] && keysPressed[2])
            player.setDirection(9);
        else if (keysPressed[0])
            player.setDirection(2);
        else if (keysPressed[1])
            player.setDirection(4);
        else if (keysPressed[2])
            player.setDirection(6);
        else if (keysPressed[3])
            player.setDirection(8);
        else
            player.setDirection(5);
    }
}
