import javax.swing.*;

class Score extends JLabel {
    private int points;

    Score() {
        setEnabled(false);
        setText("0 points");
    }

    void incrementPoints() {
        points++;
        setText(points + " points");
    }

}
