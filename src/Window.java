import javax.swing.*;
import java.awt.*;

class Window extends JFrame {

    Window(Canvas canvas, Score score) {
        setSize(500, 550);
        BorderLayout borderLayout = new BorderLayout(0, 10);
        setLayout(borderLayout);
        add(score, BorderLayout.PAGE_START);
        add(canvas, BorderLayout.CENTER);
        setVisible(true);
    }
}
